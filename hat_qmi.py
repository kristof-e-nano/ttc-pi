#!/usr/bin/python

import os
import time

last_pass = time.time()

def enable_gprs():
	try:
		print('Startup wait.')
		time.sleep(2)
		command = 'sudo qmicli -d /dev/cdc-wdm0 --dms-set-operating-mode="online"'
		os.system(command)
		print('QMI set operating mode online.')
		time.sleep(1)
		command = 'sudo ip link set wwan0 down'
		os.system(command)
		print('Stop wwan0 interface.')
		time.sleep(1)
		command = 'echo "Y" | sudo tee /sys/class/net/wwan0/qmi/raw_ip'
		os.system(command)
		print('Use rawn IP.')
		time.sleep(1)
		command = 'sudo ip link set wwan0 up'
		os.system(command)
		print('Start wwan0 interface.')
		time.sleep(1)
		command = "sudo qmicli --device=/dev/cdc-wdm0 --device-open-proxy --wds-start-network='ip-type=4,apn=everywhere' --client-no-release-cid"
		os.system(command)
		print('QMI start network.')
		time.sleep(1)
		command = 'sudo udhcpc -i wwan0'
		os.system(command)
		print('Default route.')
		time.sleep(1)
		command = 'ip a s wwan0'
		os.system(command)
		print('Get DHCP lease.')
		time.sleep(1)
		print('Connection success.')

	except Exception as e:
		println(e)
		println('Error conecting.')

enable_gprs()
